// See http://kb.mozillazine.org/Localize_extension_descriptions
pref("extensions.printpdf.net.description", "chrome://printpdf/locale/printpdf.properties");

// Added by bho
pref("extensions.printpdf.print.showBGColors",  true);
pref("extensions.printpdf.print.showBGImages", false);
pref("extensions.printpdf.print.showHeaders",  false);
