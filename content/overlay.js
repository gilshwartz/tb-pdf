var printpdf = {
  onLoad: function() {
    // initialization code
    this.initialized = true;
    this.strings     = document.getElementById("printpdf-strings");
    this.prefs       = Components.classes["@mozilla.org/preferences-service;1"]
                              .getService(Components.interfaces.nsIPrefService).getBranch("extensions.printpdf.");
  },
  onMenuItemCommand: function(e) {
    var nsIFilePicker = Components.interfaces.nsIFilePicker;
    var picker = Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);
    picker.init(window, "Save Document as PDF", nsIFilePicker.modeSave);
    picker.appendFilter("PDF", "*.pdf");
    picker.defaultExtension = "pdf";
    picker.defaultString = content.document.title;

    var path = picker.show();
	
	if (path) {
		// Canceled
		return;
	}

    var webBrowserPrint = window.content.QueryInterface(Components.interfaces.nsIInterfaceRequestor)
    .getInterface(Components.interfaces.nsIWebBrowserPrint);

    var PSSVC = Components.classes["@mozilla.org/gfx/printsettings-service;1"]
    .getService(Components.interfaces.nsIPrintSettingsService);

    var printSettings = PSSVC.newPrintSettings;
    
    printSettings.printToFile = true;
    printSettings.toFileName  = picker.file.path+'.pdf';
    printSettings.printSilent = true;
    printSettings.outputFormat = Components.interfaces.nsIPrintSettings.kOutputFormatPDF;
    
    // Added by bho
    var myPrintPrefs  = printpdf.getPrintingPrefs();
    printSettings.printBGColors   = myPrintPrefs.showBGColor;
    printSettings.printBGImages   = myPrintPrefs.showBGImages;
    
    if(myPrintPrefs.showHeaders === false) {
        //lets hide those ugly default headers
        printSettings.footerStrCenter = '';
        printSettings.footerStrLeft   = '';
        printSettings.footerStrRight  = '';
        printSettings.headerStrCenter = '';
        printSettings.headerStrLeft   = '';
        printSettings.headerStrRight  = '';
    }
    // END: Added by bho

    webBrowserPrint.print(printSettings, null);
  },
  onToolbarButtonCommand: function(e) {
    // just reuse the function above.  you can change this, obviously!
    printpdf.onMenuItemCommand(e);
  },
  getPrintingPrefs: function (){
      return {
          showBGColor: this.prefs.getBoolPref("print.showBGColors"),
          showBGImages: this.prefs.getBoolPref("print.showBGImages"),
          showHeaders: this.prefs.getBoolPref("print.showHeaders")
      };
  }

};
window.addEventListener("load", function(e) { printpdf.onLoad(e); }, false);
