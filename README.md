Print to PDF
============

This extension for Thunderbird 24+ adds a "Print to PDF" entry into
the File menu to allow printing of an email to PDF.

It is a modified version of the printpdf Firefox add-on by Stuart Parmenter and Brad Horrocks.

The xpi directory has a ready to download and install .xpi file.